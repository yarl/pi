#!/bin/sh
if ! grep -qE '^dtoverlay=my' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
	echo "Adding 'dtoverlay=my' to config.txt (9dof stuff)."
	cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# 9dof
dtoverlay=my
__EOF__
fi



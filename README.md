### rpi b rev2 + lsm303dlhc + l3g4200d + adafruit ultimate gps

let's call the ensemble of 3 othogonals accelerometers + 3 orthogonals magnetometers + 3 orthogonals gyroscopes "9dof"

The 9dof is one chip, with 7 pins (I ignore what I don't need).
- power: 3.3V and gnd
- i2c: clock and data
- 3 interrupt pins (data ready):
  - int accel <--> gpio 17
  - int magn <--> gpio 27
  - int gyro <--> gpio 22

see [diagramme](diagramme.txt)

buildroot, kernel from https://github.com/raspberrypi/linux with iio enabled.

I use st_sensors modules for accelerometer and gyroscope.

I wrote a module for the magnetometer because the one from the kernel (st_sensors) doest not implement
interrupt triggered buffer (for that magnetometer).  
Pretty poorly documented magnetometer, poorly implemented interrupt stuff (on the chip), almost no configuration..  
NO WARRANTY  
I don't share it because too bad. I don't improve it because lazy (for now).  
see 0001-lsm303dlhc-magnetometer-dirtiest-module.patch  

I add a "buildroot overlay", what's in 'buildroot-overlay' directory.
- /etc/init.d/S50myinit to modprobe relevant modules

what's in 'dt-overlays' directory will go in fat partition's overlay directory. It contains "devicetree overlays" to
configure gpio and interrupts. see also post-image.sh dirty stuff

my-overlay.dts is the source, compiled into my-overlay.dtb using the dtc compiler from 
https://github.com/raspberrypi/linux. If the clone's path is in linuxrpi shell variable:  
$linuxrpi/scripts/dtc/dtc -I dts -O dtb -o my-overlay.dtb my-overlay.dts
I compiled myself bcm2708-rpi-b.dtb ('make ARCH=arm CROSS_COMPILE=arm-linux-gnu- dtbs' in the kernel tree) because
the ethernet is buggy with the one fetched by buildroot from https://github.com/raspberrypi/

Again, I don't share it (to buildroot) for same reasons. 

that overlay is loaded by adding dtoverlay=my in config.txt. This job's done by post-build.sh

buildroot's config is in buildroot-config

the rpi linux kernel config is in kernel-config

all this is dirty

just ./executeme the first time

On first boot, do not connect gps. There's a tty (on purpose) on serial. Ethernet can be buggy..

`mount /dev/mmcblk0p1 /mnt`

remove `console=tty1 console=ttyAMA0,115200` in /mnt/cmdline.txt  

remove
```
#Put a getty on the serial port
ttyAMA0::respawn:/sbin/getty -L  ttyAMA0 115200 vt100 # GENERIC_SERIAL
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console
```
from /etc/inittab , then `reboot`

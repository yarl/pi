#!/bin/sh
echo $(pwd)
echo "$BINARIES_DIR"
rm -rf "$BINARIES_DIR/overlays"
cp -vr ../dt-overlays "$BINARIES_DIR/overlays"
cp -v ../bcm2708-rpi-b.dtb "$BINARIES_DIR/rpi-firmware"
cp -v ../genimage-raspberrypi.cfg board/raspberrypi/
